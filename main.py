import tkinter as tk
from tkinter import ttk
from gui.mapview import MapViewer
from gui.pointlist import PointList
from gui.pointviewer import PointViewer
from gui.measurementviewer import MeasurementViewer
from gui.filemenu import FileMenu
from gui.aplist import APList
from gui.localizer import Localizer
from gui.graphmap import Graphmap
from lib.point import Point

from lib.reader import PointManager


class Application(ttk.Frame):

    def __init__(self, master):
        ttk.Frame.__init__(self, master)
        self.pack(padx=20, pady=20)

        self.winfo_toplevel().title("PWS")

        self.point_manager = PointManager()

        self.size = (76.1, 67) # 66
        self.picture = 'plattegrond.jpg'
        # self.size=(20, 20)
        # self.picture = 'grond.jpg'

        # self.map_viewer = MapViewer(self, self.picture, self.size[0], self.size[1], self.point_manager)
        # self.map_viewer.grid(row=0, column=0, rowspan=2)

        self.graph_map = Graphmap(self, self.point_manager, self.size, self.picture)
        self.graph_map.grid(row=0, column=0, rowspan=3)

        self.point_list = PointList(self, self.point_manager)
        self.point_list.grid(row=0, column=1)

        self.file_menu = FileMenu(self, self.point_manager)
        self.file_menu.grid(row=1, column=1)

        self.localizer = Localizer(self, self.point_manager)
        self.localizer.grid(row=2, column=1)

        self.point_viewer = PointViewer(self, self.point_manager)
        self.point_viewer.grid(row=1, column=2)

        self.measurement_viewer = MeasurementViewer(self, self.point_manager, self.size, self.graph_map.draw_localize_rect)
        self.measurement_viewer.grid(row=2, column=2)

        self.ap_list = APList(self, self.point_manager, self.graph_map.draw_all)
        self.ap_list.grid(row=0, column=2)

        self.point_manager.add(Point(0,0,[]))
        self.point_manager.open('cb2.pws')
        # self.point_manager.open('grond.pws')


root = tk.Tk()

app = Application(root)

root.mainloop()
