from lib.point import Point
from lib.measurement import Measurement
import json

class PointManager():
    def __init__(self):
        self.listeners = []
        self.selection_listeners = []
        self.points = {}
        self.selected = ''

    def save(self, file):
        root = []
        for point in list(self.points.values()):
            m = []
            for measurement in point.measurements:
                m.append({
                    'ssid': measurement.ssid,
                    'bssid': measurement.bssid,
                    'signal': measurement.signal
                })
            p = {
                'x': point.x,
                'y': point.y,
                'measurements': m
            }
            root.append(p)

        with open(file, 'w') as outf:
            json.dump(root, outf)

    def open(self, file):
        with open(file) as inf:
            data = json.load(inf)
            points = []
            for p in data:
                measurements = []
                for m in p['measurements']:
                    measurement = Measurement(m['ssid'], m['bssid'], m['signal'])
                    measurements.append(measurement)
                point = Point(p['x'], p['y'], measurements)
                points.append(point)
            self.points = {}
            print("Loaded {} points".format(len(points)))
            self.set_points(points)

    def remove_dup_measurements(self):
        ## Remove dup measurements
        for point_id, point in self.points.items():
            bssids = []
            unique_measurements = []
            for m in point.measurements:
                if m.bssid not in bssids:
                    unique_measurements.append(m)
                    bssids.append(m.bssid)
            self.points[point_id].measurements = unique_measurements

    def add(self, point):
        self.points[point.get_id()] = point
        self.notify()

    def remove(self, id):
        del self.points[id]
        self.notify()

    def set_selected(self, id):
        self.selected = id
        for c in self.selection_listeners:
            c(id)

    def set_points(self, points):
        for point in points:
            self.points[point.get_id()] = point
        self.notify()

    def notify(self):
        for c in self.listeners:
            c(self.points.values())

    def listen(self, callback):
        self.listeners.append(callback)

    def listen_for_select(self, callback):
        self.selection_listeners.append(callback)