import math
from lib.localizing import Localizing
import numpy as np

class CalculatedPoint:
    def __init__(self, pos, n_signal_maps, success, surface, calculated_pos=''):
        self.pos = pos
        self.calculated_pos = calculated_pos
        self.n_signal_maps = n_signal_maps
        self.success = success
        self.surface = surface
        self.deviation = np.NaN

        if self.success:
            self.diff_x = pos[0] - calculated_pos[0]
            self.diff_y = pos[1] - calculated_pos[1]
            diff = math.sqrt(self.diff_x ** 2 + self.diff_y ** 2)
            self.deviation = diff

class Deviation:

    def __init__(self):
        self.localizing = Localizing()

    def mean_deviation(self, points, size, ppm):
        deviations = []
        calculated_points = []
        to_go = len(points)
        i = 0
        failed = 0
        most_dev = 0
        for p in points:
            i += 1
            # get signals on point
            signals = {}
            for m in p.measurements:
                signals[m.bssid] = m.signal

            # set points for calculations
            self.localizing.set_points(points, exclude_id=p.get_id())

            # find rectangle and build signal maps in rectangle
            signal_maps, grid_x, grid_y, rectangle, rectangles = self.localizing.build_intersect_signal_maps(signals, size, points_per_meter=ppm)
            surface = rectangle[2] * rectangle[3]

            if len(signal_maps) < 1:
                failed += 1
                calculated_point = CalculatedPoint((p.x, p.y), len(signal_maps), False, surface)
                calculated_points.append(calculated_point)
                continue

            # calculate total diff map
            total_diff, _ = self.localizing.build_diff_map(signal_maps, signals)

            # get lowest cost
            lowest_point = self.localizing.get_lowest_coordinate(total_diff, grid_x, grid_y)

            # log calculated point for analytics
            calculated_point = CalculatedPoint((p.x, p.y), len(signal_maps), True, surface, calculated_pos=lowest_point)

            calculated_points.append(calculated_point)
            deviations.append(calculated_point.deviation)

            progress = i/to_go * 100
            avg = np.average(deviations)
            std = np.std(deviations)
            min_diff = min(deviations)
            max_diff = max(deviations)
            if max_diff > most_dev:
                most_dev = max_diff
                print(p.x, p.y, max_diff)

            print("{:.2f}%, {:.2f}m avg, {:.2f}m std, {:.2f}m min diff, {:.2f}m max diff, {}x failed, {} maps used, on {:.2f}m2".format(progress, avg, std, min_diff, max_diff, failed, len(signal_maps), surface))
            # print(round(i/to_go * 100,2), round(diff, 2), round(np.average(deviations), 2))
        print("Failure rate: {:.4f}%".format(failed/to_go * 100))
        return calculated_points

    def remove_outliers(self, calculated_points):
        deviations = list([cp.deviation for cp in calculated_points if cp.success])
        avg = np.average(deviations)
        std = np.std(deviations)
        inliers = []
        for cp in calculated_points:
            if not cp.success:
                inliers.append(cp)
            if (cp.deviation < avg + (2 * std)) and (cp.deviation > avg - (2 * std)):
                inliers.append(cp)
        print("Removed {} outliers".format(len(calculated_points) - len(inliers)))
        return inliers
