import uuid

class Point():
    def __init__(self, x, y, measurements):
        self.x = x
        self.y = y
        self.measurements = measurements
        self.__id = str(uuid.uuid4())

    def get_id(self):
        return self.__id