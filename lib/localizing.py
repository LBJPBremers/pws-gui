from lib.fitting import Fitting
import numpy as np


class Localizing:
    def __init__(self):
        self.fitting = Fitting()
        self._points = []

    def set_points(self, points, exclude_id = ''):
        if exclude_id != '':
            points = self.exclude_point(points, exclude_id)
        self._points = points

    def exclude_point(self, points, id):
        points = points.copy()
        for i, p in enumerate(points):
            if p.get_id() == id:
                del points[i]
        return points

    def add_margin(self, rectangle, margin):
        x_min = rectangle[0] - margin
        y_min = rectangle[1] - margin
        x_max = x_min + rectangle[2] + (margin * 2)
        y_max = y_min + rectangle[3] + (margin * 2)
        return x_min, x_max, y_min, y_max

    def build_signal_map(self, bssid):
        signals = self.fitting.get_measurements(bssid, self._points)
        if len(signals) < 2:
            return None, None
        x, y, z = self.fitting.explode_signals(signals)
        interpolation_func = self.fitting.rad_func(x, y, z)


        x_min, x_max = min(x), max(x)
        y_min, y_max = min(y), max(y)
        rectangle = (x_min, y_min, x_max - x_min, y_max - y_min)
        x_min, x_max, y_min, y_max = self.add_margin(rectangle, 5)


        rectangle = (x_min, y_min, x_max - x_min, y_max - y_min)  # x, y, width, height
        return interpolation_func, rectangle

    def build_intersect_signal_maps(self, signals, size, points_per_meter = 1):
        # build interpolation functions and rectangles
        interpolation_functions = {}
        rectangles = {}
        for bssid in signals:
            interp_func, rectangle = self.build_signal_map(bssid)
            if interp_func is None:
                continue
            interpolation_functions[bssid] = interp_func
            rectangles[bssid] = rectangle

        # find intersect for rectangles
        rectangle = (0, 0, size[0], size[1])
        for r in rectangles.values():
            rectangle = self.intersection(rectangle, r)
            if len(rectangle) < 4:
                rectangle = (0,0,size[0],size[1])
                break

        # rectangle = (0, 0, size[0], size[1])

        # build grid for rectangle with resolution
        points_per_meter = points_per_meter

        margin = 10

        x_min, x_max, y_min, y_max = self.add_margin(rectangle, margin)

        # x_min = rectangle[0] - margin
        # x_max = x_min + rectangle[2] + (margin * 2)
        # y_min = rectangle[1] - margin
        # y_max = y_min + rectangle[3] + (margin * 2)

        x_steps = max(2, int((x_max - x_min) * points_per_meter))
        y_steps = max(2, int((y_max - y_min) * points_per_meter))

        xi = np.linspace(x_min, x_max, x_steps)
        yi = np.linspace(y_min, y_max, y_steps)
        grid_x, grid_y = np.meshgrid(xi, yi)

        # build signal maps
        signal_maps = {}
        for bssid, func in interpolation_functions.items():
            zi = np.array(func(grid_x, grid_y)).flatten()
            signal_maps[bssid] = zi

        return (signal_maps, np.array(grid_x), np.array(grid_y), rectangle, list(rectangles.values()))

    def build_signal_diff_map(self, signal_map, signal):
        return np.absolute(signal_map - signal)

    def build_diff_map(self, signal_maps, signals):
        length = next(iter(signal_maps.values())).size
        total_diff = np.full(length, 0)
        diff_maps = {}
        for bssid in signal_maps.keys():
            signal_map = signal_maps[bssid]
            signal = signals[bssid]
            diff_map = self.build_signal_diff_map(signal_map, signal)
            total_diff = np.add(total_diff, diff_map)
            diff_maps[bssid] = diff_map

        return total_diff, diff_maps


    def intersection(self, a, b):
        x = max(a[0], b[0])
        y = max(a[1], b[1])
        w = min(a[0] + a[2], b[0] + b[2]) - x
        h = min(a[1] + a[3], b[1] + b[3]) - y
        if w < 0 or h < 0: return ()  # or (0,0,0,0) ?
        return (x, y, w, h)

    def get_lowest_coordinate(self, total_diff, grid_x, grid_y):
        lowest = np.argmin(total_diff)
        return grid_x.flatten()[lowest], grid_y.flatten()[lowest]