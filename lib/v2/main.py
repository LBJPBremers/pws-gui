from lib.reader import PointManager
from lib.v2.analyzer import Analyzer
from lib.v2.plotter import plot_basic_analytics, plot_n_signal_map_distribution, plot_ppm_mean_deviations, \
    plot_ppm_duration, plot_map_surface_duration, plot_map_surface_mean_deviation, plot_n_signal_maps_deviation, \
    plot_n_points_deviation, plot_calc_points_on_map
from lib.v2.results import save_localizer_results, load_localizer_results
from lib.v2.runner import Runner
import numpy as np


def get_file(ppm):
    return 'results/cb2.pws.{:.1f}m.json'.format(ppm)


def generate(file, ppm):
    ## Load points
    point_manager = PointManager()
    point_manager.open("../../cb2.pws")
    point_manager.remove_dup_measurements()

    ## Parse points
    size = (76.1, 67)  # size of floor of building
    localizer = Runner(point_manager.points, size)

    ## run localizer
    locations = localizer.localize_all_points(pixels_per_meter=ppm, randomize_n_points=True)

    ## save localizer results
    save_localizer_results(file, locations)


def stats(file, suffix='', ppm=1):
    ## load results
    results = load_localizer_results(file)

    ## Analyze results
    analyser = Analyzer(results)
    analyser.remove_outliers(sigma=2)
    std = analyser.calc_std()
    average = analyser.calc_avarage()
    print("avg: {:.2f}, std: {:.2f}".format(average, std))

    ## generate basic distribution plot
    # target = "results/deviation_distribution.{:.2f}.ppm.svg".format(ppm)
    # plot_basic_analytics(analyser.sort_distances(), average, std, resolution=0.25, title_suffix=suffix, save=target)

    ## generate n_signal_maps distribution
    # n_signal_map_distribution = analyser.n_signal_map_distribution()
    # plot_n_signal_map_distribution(n_signal_map_distribution, average, std)

    # sorted_n_signal_map = analyser.sort_on_signal_maps()
    # plot_n_signal_maps_deviation(sorted_n_signal_map, ppm)

    sorted_n_points = analyser.sort_on_n_points()
    plot_n_points_deviation(sorted_n_points, ppm, 'results/n_points_deviation.svg')

    # file = 'results/vectormap.{:.2f}.svg'.format(ppm)
    # plot_calc_points_on_map(results, '../../plattegrond.jpg', (76.1, 67), average, std, 'Afwijkingsvectoren (pixels per meter: {})'.format(ppm), file)

    return analyser


def ppm_stats(ppms):
    ppm_stats_dic = {}
    for ppm in ppms:
        file = get_file(ppm)
        analyzer = stats(file, '(pixels per meter: {:.2f})'.format(ppm))
        ppm_stats_dic[ppm] = analyzer.get_analysis()

    plot_ppm_mean_deviations(ppm_stats_dic)
    plot_ppm_duration(ppm_stats_dic)


def map_surface_stats():
    file = get_file(1)
    analyzer = stats(file)

    plot_map_surface_duration(analyzer.results, 1)
    # plot_map_surface_mean_deviation(analyzer.results, 1)


# generate("results/0.8.ppm.limited.points.json", 0.8)

ppms = np.arange(0.1, 1.1, 0.1)

# map_surface_stats()

stats("results/0.8.ppm.limited.points.json", ppm=0.8)


# for ppm in ppms:
#     file = 'results/cb2.pws.{:.1f}m.json'.format(ppm)
#     print(file)
#     # generate(file, ppm)
#     stats(file, '(pixels per meter: {:.2f})'.format(ppm), ppm)
