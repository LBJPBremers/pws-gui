import json


class Result:
    def __init__(self, measurements_n, signal_maps_n, diff_maps_n, x, y, distance, actual_x, actual_y, map_surface, duration, n_points):
        self.measurements_n = measurements_n
        self.signal_maps_n = signal_maps_n
        self.diff_maps_n = diff_maps_n
        self.x = x
        self.y = y
        self.distance = distance
        self.actual_x = actual_x
        self.actual_y = actual_y
        self.map_surface = map_surface
        self.duration = duration
        self.n_points = n_points


def save_localizer_results(file, locations):
    root = []
    for location in locations.values():
        root.append({
            "measurements": len(location.measurements),
            "signal_maps_n": len(location.signal_maps),
            "diff_maps_n": len(location.diff_maps),
            "x": location.x,
            "y": location.y,
            "distance": location.distance,
            "actual_x": location.actual_x,
            "actual_y": location.actual_y,
            "map_surface": location.map_box.surface(),
            "duration": location.duration,
            "n_points": location.n_points
        })
    with open(file, 'w') as outf:
        json.dump(root, outf)


def load_localizer_results(file):
    with open(file) as inf:
        data = json.load(inf)
        results = []
        for l in data:
            if "n_points" in l:
                n_points = l["n_points"]
            else :
                n_points = -1
            results.append(
                Result(l["measurements"], l["signal_maps_n"], l["diff_maps_n"], l["x"], l["y"], l["distance"],
                       l["actual_x"], l["actual_y"], l["map_surface"], l["duration"], n_points))
        return results
