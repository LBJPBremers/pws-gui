from lib.v2.localizer import Localizer, LocalizerOutput
from lib.v2.signal_maps import SignalMapManager
import time
import datetime
import numpy as np


class Runner:
    def __init__(self, points, floor_size):
        self.points = points
        self.floor_size = floor_size

    def localize_all_points(self, stop_after_x=-1, pixels_per_meter=1, randomize_n_bssids=False,
                            randomize_n_points=False):
        locations = {}
        i = 0
        total = len(self.points)
        failed = 0
        start_time = time.time()
        for point_id in self.points.keys():
            calc_start_time = time.time()
            i += 1
            if i > stop_after_x and stop_after_x != -1:
                break
            location,_ = self.localize(point_id, pixels_per_meter=pixels_per_meter, randomize_n_bssids=randomize_n_bssids,
                                     randomize_n_points=randomize_n_points)
            calc_time = time.time() - calc_start_time
            if location == None:
                failed += 1
                continue
            locations[point_id] = location
            progress = i / total
            elapsed_time = time.time() - start_time
            time_to_go = str(datetime.timedelta(seconds=(elapsed_time / progress) - elapsed_time))
            print(
                "Finished locating point: {:.2f}m in {:.2f}ms with {} points (surface: {:.2f}m2) ({}/{})({:.2f}%) ({}s elapsed - {}s to go)".format(
                    location.distance, location.duration * 1000, location.n_points, location.map_box.surface(), i, total, progress * 100,
                    str(datetime.timedelta(seconds=elapsed_time)), time_to_go))
        print("Failed {} times".format(failed))
        return locations

    def localize(self, point_id, pixels_per_meter=1, randomize_n_bssids=False, randomize_n_points=False):
        point_to_localize = self.points[point_id]

        ## remove point from point list
        current_points = self.points.copy()
        current_points.pop(point_id)

        if randomize_n_points:
            old_points = current_points.copy()
            n_measurements = np.random.randint(10, len(current_points))
            selected_keys = np.random.choice(list(current_points.keys()), n_measurements)
            current_points = {}
            for key in selected_keys:
                current_points[key] = old_points[key]

        ## create map manager
        signal_map_manager = SignalMapManager(list(current_points.values()), self.floor_size)

        ## find intersecting box
        bssids = [measurement.bssid for measurement in point_to_localize.measurements]
        if randomize_n_bssids and len(bssids) > 1:
            bssids = np.random.choice(bssids, np.random.randint(1, max(2, len(bssids))))
        map_box, n_used = signal_map_manager.find_intersecting_box(bssids)

        ## build signal maps (only those who are recieved)
        for bssid in bssids:
            signal_map_manager.build_signal_map(bssid, pixels_per_meter, save=True, map_box=map_box)

        ## localize
        localizer = Localizer(signal_map_manager, map_box, len(current_points))
        try:
            location = localizer.localize(point_to_localize.measurements)
        except Exception as e:
            print("Error localizing point: {}".format(e))
            return None
        location.calc_distance(point_to_localize.x, point_to_localize.y)

        return location, localizer
