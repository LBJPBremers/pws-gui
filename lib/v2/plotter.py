import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt

def plot_basic_analytics(sorted_distances, average, std, resolution=1, title_suffix='', save=None):
    # fit = stats.norm.pdf(sorted_distances, average, std)
    # plt.plot(sorted_distances, fit, '-o')
    bins = np.arange(min(sorted_distances), max(sorted_distances) + resolution, resolution)
    plt.hist(sorted_distances, density=True, bins=bins)
    plt.xlabel("Afwijking (m)")
    plt.ylabel("% van voorkomen van afwijking (100:1)")
    plt.title("Verdeling van afwijking (m)" + title_suffix)
    if save is None:
        plt.show()
    else:
        plt.savefig(save)
        plt.clf()


def plot_n_signal_map_distribution(sorted_n_signal_map_distribution, average, std, resolution=1):
    fit = stats.norm.pdf(sorted_n_signal_map_distribution, average, std)
    plt.plot(sorted_n_signal_map_distribution, fit, '-o')
    bins = np.arange(min(sorted_n_signal_map_distribution), max(sorted_n_signal_map_distribution) + resolution, resolution)
    plt.hist(sorted_n_signal_map_distribution, density=True, bins=bins)
    plt.xlabel("Aantal signaalkaarten gebruikt voor lokalisering")
    plt.ylabel("NormalPDF")
    plt.title("Verdeling van aantal signalmaps")
    plt.show()


def plot_ppm_mean_deviations(ppms):
    for ppm, analysis in ppms.items():
        plt.errorbar(ppm, analysis.deviation_avg, yerr=analysis.deviation_std, fmt='-o')

    plt.xlabel('Signaalkaart resolutie (pixels per meter)')
    plt.ylabel('Afwijking μ & σ (m)')
    plt.title('Verband tussen afwijking en de resolutie van de gebruikte signaalkaarten')
    plt.show()


def plot_ppm_duration(ppms):
    for ppm, analysis in ppms.items():
        plt.errorbar(ppm, analysis.duration_avg, yerr=analysis.duration_std, fmt='-o')

    plt.xlabel('Signaalkaart resolutie (pixels per meter)')
    plt.ylabel('Gemiddelde locatiebepalingstijd (s)')
    plt.title('Verband tussen locatiebepalingstijd\n en de resolutie van de gebruikte signaalkaarten')
    plt.show()


def plot_map_surface_duration(results, ppm):
    for result in results:
        plt.scatter(result.map_surface, result.duration)
    plt.xlabel("Oppervlakte van kansraster")
    plt.ylabel("Gemiddelde locatiebepalingstijd (s)")
    plt.title("Verdeling van locatiebepalingstijd bij ppm={}".format(ppm))
    plt.show()


def plot_map_surface_mean_deviation(results, ppm):
    for result in results:
        plt.scatter(result.map_surface, result.distance)
    plt.xlabel("Oppervlakte van kansraster")
    plt.ylabel("Afwijking μ & σ (m)")
    plt.title("Verdeling van afwijking tegen kansraster opppervlakte bij ppm={}".format(ppm))
    plt.show()


def plot_n_signal_maps_deviation(sorted_n_signal_maps, ppm):
    for n_signal_maps, results in sorted_n_signal_maps.items():
        distances = [result.distance for result in results]
        avg = np.average(distances)
        std = np.std(distances)
        plt.errorbar(n_signal_maps, avg, yerr=std, fmt='-o')
    plt.xlabel("Aantal AP's ontvangen")
    plt.ylabel("Afwijking μ & σ (m)")
    plt.title("Gemiddelde afwijking tegen het aantal AP's ontvangen bij ppm={}".format(ppm))
    plt.show()


def plot_n_points_deviation(sorted_n_points, ppm, file):
    training = []
    for n_signal_maps, results in sorted_n_points.items():
        if len(results) < 2:
            continue
        distances = [result.distance for result in results]
        avg = np.average(distances)
        training.append([n_signal_maps, avg])
        std = np.std(distances)
        plt.errorbar(n_signal_maps, avg, yerr=std, fmt='-o', alpha=0.2)

    training.sort(key=lambda x: x[0])
    training = np.array(training)
    x = training[:,0]
    y = training[:,1]
    z = np.polyfit(x, y, 3)
    trend = np.poly1d(z)(x)
    plt.plot(x, trend, 'b-')
    plt.xlabel("Aantal meetpunten")
    plt.ylabel("Afwijking μ & σ (m)")
    plt.title("Verband tussen de afwijking en het aantal meetpunten (p/m: {}".format(ppm))
    plt.savefig(file)
    plt.clf()
    # plt.show()

def plot_calc_points_on_map(results, map, size, avg, std, title, file, sigma=2):
    img = plt.imread(map)
    plt.imshow(img, origin='lower', extent=(0, size[0], 0, size[1]))

    results = [x for x in results if (x.distance > avg - sigma * std)]
    results = [x for x in results if (x.distance < avg + sigma * std)]

    x, y, dx, dy, c = [], [], [], [], []
    for r in results:
        ax, ay = r.actual_x, r.actual_y
        rdx, rdy = r.x - ax, r.y - ay

        x.append(r.actual_x)
        y.append(r.actual_y)

        dx.append(rdx)
        dy.append(rdy)

        c.append(r.distance)

    plt.gca().invert_yaxis()
    # plt.scatter(x, y, c=e)
    plt.quiver(x, y, dx, dy, c)
    plt.title(title)

    # plt.tricontourf(x, y, c)
    plt.savefig(file)
    plt.clf()