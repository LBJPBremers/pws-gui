import numpy as np
from scipy import stats

class Analyzer:
    def __init__(self, results):
        self.results = results
        self.distances = list([result.distance for result in self.results])
        self.durations = list([result.duration for result in self.results])
        self.map_surfaces = list([result.map_surface for result in self.results])

    def calc_std(self):
        return np.std(self.distances)

    def calc_avarage(self):
        return np.average(self.distances)

    def calc_std_durations(self):
        return np.std(self.durations)

    def calc_avarage_durations(self):
        return np.average(self.durations)

    def calc_std_map_surface(self):
        return np.std(self.map_surfaces)

    def calc_avarage_map_surface(self):
        return np.average(self.map_surfaces)

    def sort_distances(self):
        return np.sort(self.distances)

    def get_analysis(self):
        return Analysis(self.calc_std(), self.calc_avarage(), self.calc_std_durations(), self.calc_avarage_durations())

    # def bin(self, bin_axis_key, bins):
    #     bin_axis = list([getattr(result, bin_axis_key) for result in self.results])
    #     bin_min, bin_max = min(bin_axis), max(bin_axis)
    #     step = (bin_max - bin_min) / bins
    #     bins = np.arange(bin_min, bin_max, step)
    #     bin_numbers = np.digitize(bin_axis, bins)
    #     print(bins, bin_max + step)
    #
    # def bin_stats(self, bin_axis_key, value_axis_key, stat_func, bins):
    #     bin_axis = []
    #     values = []
    #     for result in self.results:
    #         bin_axis.append(getattr(result, bin_axis_key))
    #         values.append(getattr(result, value_axis_key))
    #     print(values)
    #     values, bin_edges, _ = stats.binned_statistic(bin_axis, values, stat_func, bins)
    #     bin_width = bin_edges[1] - bin_edges[0]
    #     bin_centers = bin_edges[1:] - bin_width / 2
    #     return values, bin_width, bin_centers

    def remove_outliers(self, sigma=2):
        final_list = [x for x in self.distances if (x > self.calc_avarage() - sigma * self.calc_std())]
        final_list = [x for x in final_list if (x < self.calc_avarage() + sigma * self.calc_std())]
        self.distances = list(final_list)

    def sort_on_signal_maps(self):
        sorted_on_signal_maps = {}
        for result in self.results:
            if result.signal_maps_n in sorted_on_signal_maps:
                sorted_on_signal_maps[result.signal_maps_n].append(result)
            else:
                sorted_on_signal_maps[result.signal_maps_n] = [result]

        return sorted_on_signal_maps

    def sort_on_n_points(self):
        sorted_on_n_points = {}
        for result in self.results:
            if result.n_points in sorted_on_n_points:
                sorted_on_n_points[result.n_points].append(result)
            else:
                sorted_on_n_points[result.n_points] = [result]

        return sorted_on_n_points

    def n_signal_map_distribution(self, sorted_on_signal_maps=None):
        if sorted_on_signal_maps == None:
            sorted_on_signal_maps = self.sort_on_signal_maps()

        distribution = []
        for n_signal_map, results in sorted_on_signal_maps.items():
            for result in results:
                distribution.append(n_signal_map)

        return np.sort(distribution)

class Analysis:
    def __init__(self, deviation_std, deviation_avg, duration_std, duration_avg):
        self.deviation_std = deviation_std
        self.deviation_avg = deviation_avg
        self.duration_std = duration_std
        self.duration_avg = duration_avg