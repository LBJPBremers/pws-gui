from lib.reader import PointManager

import matplotlib

from lib.v2.localizer import Localizer
from lib.v2.runner import Runner
from lib.v2.signal_maps import SignalMapManager

# matplotlib.use('TkAgg')

from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import matplotlib.pyplot as plt
import numpy as np

## import points
point_manager = PointManager()
point_manager.open("../../../cb2.pws")
# points = np.random.choice(list(point_manager.points.values()), 5)

## create plot
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

## alternative
size = (76.1, 67)
smm = SignalMapManager(point_manager.points.values(), size)

# bssid = np.random.choice(list(smm.ap_measurements.keys()))
bssid = '40:18:b1:34:fb:e9:'
print(bssid)


# floor size
measurements = smm.ap_measurements[bssid]
xs = list([m.x for m in measurements])
ys = list([m.y for m in measurements])
x_min = min(xs) - 10
x_max = max(xs) + 10
y_min = min(ys) - 10
y_max = max(ys) + 10

# plot signalmap
map_size = (x_min, y_min, x_max, y_max)
signalMap = smm.build_signal_map(bssid, 1)
ax.plot_wireframe(signalMap.X, signalMap.Y, signalMap.Z, alpha=0.3)

# plot signal points
for ap_m in smm.ap_measurements[bssid]:
    ax.scatter(ap_m.x, ap_m.y, ap_m.signal, c='r')

# signal surface
signal = -80

x = np.linspace(x_min, x_max, 2)
y = np.linspace(y_min, y_max, 2)
xv, yv = np.meshgrid(x, y)
z = np.full(xv.shape, signal)
ax.plot_surface(xv, yv, z, alpha=0.8)

# diff map
# localizer = Localizer(smm)
# diff = localizer.build_signal_diff_grid_map(signalMap, signal)
# prop = localizer.diffToProp(diff)
#
# ax.contourf(signalMap.X, signalMap.Y, prop, 20)

# diff
for i in range(len(signalMap.x_grid)):
    px = signalMap.x_grid[i]
    py = signalMap.y_grid[i]
    pz = signalMap.z_grid[i]

    ax.plot([px, px], [py, py], [signal, -abs(pz - signal)], c='purple', alpha=0.5)

# total diff
size = (76.1, 67)  # size of floor of building
# runner = Runner(point_manager.points, size)
# point_id = np.random.choice(list(point_manager.points.keys()))
# localizerOutput, smm = runner.localize(point_id, 1)
#
# signal_maps = list(localizerOutput.signal_maps.values())
#
# signal_map = signal_maps[0]
# shape = signal_map.X.shape
# total_diff = localizerOutput.total_diff.reshape(shape)
#
# # plot distance
# ax.scatter(localizerOutput.x, localizerOutput.y)
# ax.scatter(localizerOutput.actual_x, localizerOutput.actual_y)
#
# # for diff_map in localizerOutput.diff_maps.values():
# #     data = localizer.diffToProp(diff_map.reshape(shape))
# #     ax.contourf(signal_map.X, signal_map.Y, data)
#
# data = localizer.diffToProb(total_diff, True)
# hist = localizer.propsToHist(data, 2)
# print(np.sum(data))
# chances = np.multiply(data, 100)
# ax.contourf(signal_map.X, signal_map.Y, chances, 100)
#
# ## axis
# ax.set_xlabel('Afstand (meter)')
# # ax.set_xlim([x_min, x_max])
# ax.set_ylabel('Afstand (meter)')
# # ax.set_ylim([y_min, y_max])
# ax.set_zlabel('φ (kans)')
# # ax.set_zlim([-150, -40])
#

plt.show()
