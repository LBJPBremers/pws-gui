import json, math

import matplotlib

from lib.reader import PointManager
from lib.v2.localizer import Localizer
from lib.v2.runner import Runner
from lib.v2.signal_maps import SignalMapManager

from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import matplotlib.pyplot as plt
import numpy as np

# Load data
point_manager = PointManager()
point_manager.open("measurements.json")
point_manager.remove_dup_measurements()

# Build processors
size = (76.1, 67)  # size of floor of building
runner = Runner(point_manager.points, size)

def plot_3d_map(data, title, zlabel, file):
    plt.close('all')
    plt.clf()

    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.contourf(signalmap.X, signalmap.Y, data, 50)
    ax.set_xlabel('Locatie (meter)', color='white')
    ax.tick_params(axis='x', colors='white')
    plt.gca().invert_yaxis()
    ax.set_ylabel('Locatie (meter)', color='white')
    ax.tick_params(axis='y', colors='white')
    ax.set_zlabel(zlabel, color='white')
    ax.tick_params(axis='z', colors='white')
    ax.set_title(title, color='white')
    fig.add_axes(ax)

    plt.savefig(file, transparent=True, bbox_inches='tight')

# Process each point
dataOut = []
for pId, point in point_manager.points.items():
    pos, localizer = runner.localize(pId)
    print('x: {:.2f}, y: {:.2f}, d: {:.2f}, t: {:.2f}'.format(point.x, point.y, pos.calc_distance(point.x, point.y), pos.duration * 1000))

    signalmap = pos.signal_maps[list(pos.signal_maps.keys())[0]]

    # Plot total diff
    map_shape = signalmap.Z.shape
    diffmap = np.array(pos.total_diff).reshape(map_shape)
    probMap = localizer.diffToProb(diffmap, inverse=True)

    file = 'chance/{:.2f}-{:.2f}.svg'.format(point.x, point.y)

    plot_3d_map(probMap, 'Kansenraster', 'Ψ (kans)', file)

    # Add data to points
    measurements = []
    for m in pos.measurements:
        measurements.append({
            'bssid': m.bssid,
            'ssid': m.ssid,
            'signal': m.signal
        })

    data = {
        'x': point.x,
        'y': point.y,
        'measurements': measurements,
        'calc_x': pos.x,
        'calc_y': pos.y,
        'duration': pos.duration,
        'map_box': {
            'x': pos.map_box.x,
            'y': pos.map_box.y,
            'w': pos.map_box.w,
            'h': pos.map_box.h
        }
    }
    dataOut.append(data)

with open('measurements.json', 'w') as f:
    json.dump(dataOut, f)