from lib.reader import PointManager

import json

nope = ['40:18:b1:33:59:ea:', '40:18:b1:32:88:16:', '40:18:b1:cc:5e:96:', ]

with open('signalmaps/meta.json') as f:
    maps = list(json.load(f).keys())

dataout = []
with open('../../../cb2.pws') as fin:
    dataIn = json.load(fin)
    for p in dataIn:
        newP = {'x': p['x'], 'y': p['y'], 'measurements': []}
        for m in p['measurements']:
            if m['bssid'] in maps and m['bssid'] not in nope:
                newP['measurements'].append(m)
        dataout.append(newP)

with open('measurements.json', 'w') as f:
    json.dump(dataout, f)

