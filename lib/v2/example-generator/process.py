from lib.reader import PointManager

import matplotlib

from lib.v2.localizer import Localizer
from lib.v2.runner import Runner
from lib.v2.signal_maps import SignalMapManager

matplotlib.use('TkAgg')

from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import matplotlib.pyplot as plt
import numpy as np

## import points
point_manager = PointManager()
point_manager.open("../../../cb2.pws")
point_manager.remove_dup_measurements()

## Localizer
size = (76.1, 67)  # size of floor of building
runner = Runner(point_manager.points, size)

## run localizer
# random point
point_ids = list(point_manager.points.keys())
point_id = np.random.choice(point_ids)
point_to_localize = point_manager.points[point_id]
location_output, localizer = runner.localize(point_id)
if location_output is None:
    exit(1)

## measurement dict
md = {}
for m in point_to_localize.measurements:
    md[m.bssid] = m.signal

## create plot
x_min, x_max = location_output.map_box.x, location_output.map_box.x_max()
y_min, y_max = location_output.map_box.y, location_output.map_box.y_max()

i = 0
map_shape = ()
for bssid, signalmap in location_output.signal_maps.items():
    i += 1
    fig = plt.figure()
    fig.suptitle(bssid)

    # plot signal map
    ax = fig.add_subplot(1, 2, 1, projection='3d')
    ax.contourf(signalmap.X, signalmap.Y, signalmap.Z, 50)

    # plot signal
    x = np.linspace(x_min, x_max, 2)
    y = np.linspace(y_min, y_max, 2)
    xv, yv = np.meshgrid(x, y)
    z = np.full(xv.shape, md[bssid])
    ax.plot_surface(xv, yv, z)

    ax.set_xlabel('Locatie (meter)')
    ax.set_ylabel('Locatie (meter)')
    ax.set_zlabel('Signaalsterkte (dBm)')
    ax.set_title("Signaalkaart met signaalmeting ({}dBm)".format(md[bssid]))

    # plot diff map
    ax = fig.add_subplot(1, 2, 2, projection='3d')
    map_shape = signalmap.Z.shape
    diffmap = np.array(location_output.diff_maps[bssid]).reshape(map_shape)
    ax.contourf(signalmap.X, signalmap.Y, diffmap, 50)
    ax.set_xlabel('Locatie (meter)')
    ax.set_ylabel('Locatie (meter)')
    ax.set_zlabel('Absolute verschil in signaalsterkte (dBm)')
    ax.set_title("Afwijkingskaart")

    # save
    plt.tight_layout()
    fig.savefig('./process/sig.to.diff.{}.svg'.format(i))
    plt.close()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_title("Alle afwijkingskaarten opgeteld")
diffmap = np.array(location_output.total_diff).reshape(map_shape)
ax.contourf(signalmap.X, signalmap.Y, diffmap, 50)
ax.set_xlabel('Locatie (meter)')
ax.set_ylabel('Locatie (meter)')
ax.set_zlabel('Absolute verschil in signaalsterkte (dBm)')
fig.savefig('./process/total.diff.svg')
plt.close()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_title("Kansenraster (Inversed & Normalized)")
propMap = localizer.diffToProb(diffmap, inverse=True)
ax.contourf(signalmap.X, signalmap.Y, propMap, 50)
ax.set_xlabel('Locatie (meter)')
ax.set_ylabel('Locatie (meter)')
ax.set_zlabel('φ (kans)')
fig.savefig('./process/chance.svg')
plt.close()
