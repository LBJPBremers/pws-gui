import json

from lib.reader import PointManager

with open('../results/cb2.pws.1.0m.json') as f:
    results = json.load(f)


with open('measurements.json') as fin:
    dataIn = json.load(fin)

dataout = []

for p in dataIn:
    newP = {'x': p['x'], 'y': p['y'], 'measurements': p['measurements']}
    for result in results:
        if p['x'] == result['actual_x'] and p['y'] == result['actual_y']:
            newP['calc_x'] = result['x']
            newP['calc_y'] = result['y']
            dataout.append(newP)
            break

with open('measurements.json', 'w') as f:
    json.dump(dataout, f)
