from lib.reader import PointManager
from lib.v2.signal_maps import SignalMapManager

import json

import matplotlib.pyplot as plt
plt.rcParams['axes.xmargin'] = 0
plt.rcParams['axes.ymargin'] = 0

point_manager = PointManager()
point_manager.open("../../../cb2.pws")


size = (76.1, 67)
smm = SignalMapManager(point_manager.points.values(), size)
bssid = '40:18:b1:35:60:96:'

i = 0
meta = {}
for bssid in smm.ap_measurements.keys():
    box = smm.find_box(bssid)
    map = smm.build_signal_map(bssid, 1, True, box)

    if map is None:
        continue
    if i > 1 and False:
        break

    # print(bssid, box.surface())
    # continue

    # plt.clf()
    #
    # fig = plt.figure()
    # # fig.set_size_inches(box.w / box.h, 1, forward=False)
    # ax = plt.axes()
    # ax.set_axis_off()
    # fig.add_axes(ax)
    #
    # ax.contourf(map.X, map.Y, map.Z, 20, alpha=0.8)
    # ax.xaxis.set_visible(False)
    # ax.yaxis.set_visible(False)
    # file = 'signalmaps/{}.png'.format(bssid.replace(':', ''))
    # plt.savefig(file, transparent = True, bbox_inches='tight', pad_inches = -0.1)

    meta[bssid] = {'x': box.x, 'y': box.y, 'w': box.w, 'h': box.h}
    i += 1

with open('signalmaps/meta.json', 'w') as outf:
    json.dump(meta, outf)