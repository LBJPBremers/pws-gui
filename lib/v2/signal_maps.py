from lib.v2.fitting import build_signal_map, explode_measurements


class SignalMapManager:
    def __init__(self, points, floor_size):
        self.points = points
        self.floor_size = floor_size
        self.signal_maps = {}

        self.ap_measurements = {}
        self.assign_measurements_to_ap()

    def assign_measurements_to_ap(self):
        for point in self.points:
            for m in point.measurements:
                ap_mes = SignalMapMeasurement(point.x, point.y, m.signal)
                if m.bssid in self.ap_measurements:
                    self.ap_measurements[m.bssid].append(ap_mes)
                else:
                    self.ap_measurements[m.bssid] = [ap_mes]

        dups = 0
        for bssid, measurements in self.ap_measurements.items():
            seen = []
            for i, m in enumerate(measurements):
                loc = str(m.x) + str(m.y)
                if loc in seen:
                    dups += 1
                    del measurements[i]
                seen.append(loc)



    def find_intersecting_box(self, bssids, margin=10, sub_box_margin=10):
        box = self.get_floor_box()
        i = 0
        for bssid in bssids:
            bssid_box = self.find_box(bssid)
            margin_box = bssid_box.add_margin(sub_box_margin)
            intersect = margin_box.intersection(box)
            if intersect is not None:
                box = intersect
                i += 1

        box.add_margin(margin)
        return box, i

    def get_floor_box(self):
        return Box(0, 0, self.floor_size[0], self.floor_size[1])

    def find_box(self, bssid):
        try:
            x, y, _ = explode_measurements(self.ap_measurements[bssid])
        except Exception as e:
            print('key error')
            return Box(0, 0, 0, 0)
        x_min, x_max = min(x), max(x)
        y_min, y_max = min(y), max(y)
        return Box(x_min, y_min, x_max - x_min, y_max - y_min)

    def build_all_signal_maps(self, pixels_per_meter):
        for bssid in self.ap_measurements.keys():
            self.build_signal_map(bssid, pixels_per_meter, save=True)

    def build_signal_map(self, bssid, pixels_per_meter, save = False, map_box = None):
        if map_box is None:
            map_box = self.get_floor_box()
        try:
            signal_map = build_signal_map(self.ap_measurements[bssid], map_box, pixels_per_meter)
        except Exception as e:
            return None

        if save:
            self.signal_maps[bssid] = signal_map

        return signal_map

class SignalMapMeasurement:
    def __init__(self, x, y, signal):

        self.x = x
        self.y = y
        self.signal = signal

class Box:
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def add_margin(self, margin):
        self.w = self.w + (margin * 2)
        self.h = self.h + (margin * 2)
        self.x = self.x - margin
        self.y = self.y - margin
        return self

    def x_max(self):
        return self.x + self.w

    def y_max(self):
        return self.y + self.h

    def __str__(self):
        return "x: {}, y: {}, w: {}, h: {}".format(self.x, self.y, self.w, self.h)

    def surface(self):
        return self.w * self.h

    def intersection(self, b):
        x = max(self.x, b.x)
        y = max(self.y, b.y)
        w = min(self.x + self.w, b.x + b.w) - x
        h = min(self.y + self.h, b.y + b.h) - y
        if w < 0 or h < 0: return None
        return Box(x, y, w, h)