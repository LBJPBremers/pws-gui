import numpy as np
import math
import time

class Localizer:
    def __init__(self, signal_map_manager, map_box, n_points):
        self.signal_map_manager = signal_map_manager
        self.map_box = map_box
        self.n_points = n_points

    def localize(self, measurements):
        ## look for signal maps
        signal_diff_maps = {}
        signal_maps = {}

        start = time.time()
        for m in measurements:
            signal_map = self.signal_map_manager.signal_maps.get(m.bssid, None)
            if signal_map != None:
                signal_maps[m.bssid] = signal_map
                signal_diff_maps[m.bssid] = self.build_signal_diff_map(signal_map, m.signal)

        if len(signal_maps) < 1:
            raise Exception("Failed to find usable signalmap")

        total_diff = self.build_total_diff(list(signal_diff_maps.values()))
        normDiff = self.diffToProb(total_diff)

        index = self.find_coordinate_index(normDiff)

        x, y = list(signal_maps.values())[0].get_coordinate(index)
        duration = time.time() - start

        return LocalizerOutput(measurements, signal_maps, signal_diff_maps, total_diff, x, y, self.map_box, duration, self.n_points)

    def diffToProb(self, total_diff, inverse=False):
        min = np.min(total_diff)
        total_diff = np.subtract(total_diff, min)

        # total_diff = (np.subtract(total_diff, min)) / (np.max(total_diff) - min)
        # total_diff = np.subtract(1, total_diff)
        # total_diff = np.power(total_diff, 2)
        if inverse:
            total_diff = np.subtract(1, total_diff)
            total_diff = np.subtract(total_diff, np.min(total_diff))
        total = np.sum(total_diff)
        normDiff = np.divide(total_diff, total)
        return normDiff

    def propsToHist(self, probs, downscale_factor):
        x_size, y_size = probs.shape[0], probs.shape[1]
        x_bins = math.ceil(x_size / downscale_factor)
        y_bins = math.ceil(y_size / downscale_factor)
        downscale_factor_x = x_size / x_bins
        downscale_factor_y = y_size / y_bins
        hist, _, _ = np.histogram2d(probs[0], probs[1], bins=(x_bins, y_bins))
        hist = np.kron(hist, np.ones((downscale_factor_x, downscale_factor_y)))
        print(hist.shape, x_bins, y_bins, probs.shape)
        return hist

    def build_signal_diff_map(self, signal_map, signal):
        return np.absolute(signal_map.z_grid - signal)

    def build_signal_diff_grid_map(self, signal_map, signal):
        return np.absolute(signal_map.Z - signal)

    def build_total_diff(self, signal_diff_maps):
        total_diff = np.full(signal_diff_maps[0].size, 0)
        for signal_diff_map in signal_diff_maps:
            total_diff = np.add(total_diff, signal_diff_map)
        return total_diff

    def find_coordinate_index(self, total_diff):
        return np.argmin(total_diff)

class LocalizerOutput:
    def __init__(self, measurements, signal_maps, diff_maps, total_diff, x, y, map_box, duration, n_points):
        self.measurements = measurements
        self.signal_maps = signal_maps
        self.diff_maps = diff_maps
        self.total_diff = total_diff
        self.x = x
        self.y = y
        self.map_box = map_box
        self.duration = duration
        self.n_points = n_points

    def calc_distance(self, actual_x, actual_y, save=True):
        if save:
            self.actual_x = actual_x
            self.actual_y = actual_y
        self.distance = math.hypot(actual_x - self.x, actual_y - self.y)
        return self.distance