import numpy as np
from scipy.interpolate import Rbf


class SignalMap:
    def __init__(self, X, Y, Z):
        if X.size != Y.size or X.size != Z.size:
            raise Exception("Signalmap Measurement length not the same")

        self.X = X
        self.Y = Y
        self.Z = Z

        self.x_grid = X.flatten()
        self.y_grid = Y.flatten()
        self.z_grid = Z.flatten()

    def get_size(self):
        return self.x_grid.size

    def get_coordinate(self, index):
        return self.x_grid.flatten()[index], self.y_grid.flatten()[index]


def build_signal_map(measurements, box, pixels_per_meter=1):
    # check box
    if not check_box(box):
        raise Exception('signal map box has no or a negative surface')

    # explode points (x, y, z)[] to x[], y[], z[]
    x, y, z = explode_measurements(measurements)

    # calculate resolution
    x_steps = max(2, box.w * pixels_per_meter)
    y_steps = max(2, box.h * pixels_per_meter)

    # build grid
    xx, yy = np.meshgrid(np.linspace(box.x, box.x_max(), x_steps),
                         np.linspace(box.y, box.y_max(), y_steps))

    # fit/interpolate
    try:
        fitted_func = Rbf(x, y, z, function='linear')
        # generate signal map
        zz = fitted_func(xx, yy)
        return SignalMap(xx, yy, zz)
    except Exception as e:
        # print(e, 'fit fail', x_steps, y_steps)
        return None

def explode_measurements(measurements):
    x = np.array(list((measurement.x for measurement in measurements)))
    y = np.array(list((measurement.y for measurement in measurements)))
    z = np.array(list((measurement.signal for measurement in measurements)))
    return x, y, z


def check_box(box):
    if box.w <= 0 or box.h <= 0:
        return False
    return True
