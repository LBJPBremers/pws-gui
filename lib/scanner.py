import time
import pywifi


class Scanner():
    def scan(self, interface):
        print("Start scan...", end='', flush=True)
        interface.scan()
        time.sleep(10)
        print("Done")
        return interface.scan_results()

    def get_interface(self):
        return pywifi.PyWiFi().interfaces()[0]
