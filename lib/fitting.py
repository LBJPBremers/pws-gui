import itertools
import numpy as np
import matplotlib as mpl
import matplotlib.backends.tkagg as tkagg
from matplotlib.backends.backend_agg import FigureCanvasAgg
import tkinter as tk
from scipy.interpolate import griddata
from scipy.interpolate import Rbf
from scipy import interpolate
import matplotlib.pyplot as plt

class Fitting():
    def polyfit2d(self, x, y, z, order=3):
        ncols = (order + 1) ** 2
        G = np.zeros((x.size, ncols))
        ij = itertools.product(range(order + 1), range(order + 1))
        for k, (i, j) in enumerate(ij):
            G[:, k] = x ** i * y ** j
        m, _, _, _ = np.linalg.lstsq(G, z, rcond=None)
        return m

    def polyval2d(self, x, y, m):
        order = int(np.sqrt(len(m))) - 1
        ij = itertools.product(range(order + 1), range(order + 1))
        z = np.zeros_like(x)
        for a, (i, j) in zip(m, ij):
            z += a * x ** i * y ** j
        return z

    def interpolate(self, x, y, z, meshgrid):
        return griddata((x, y), z, meshgrid, method='linear')

    def fit(self, x, y):
        nx, ny = 100, 100
        xx, yy = np.meshgrid(np.linspace(x.min(), x.max(), nx),
                             np.linspace(y.min(), y.max(), ny))

    def explode_signals(self, signals):
        x = np.array(list((signal['x'] for signal in signals)))
        y = np.array(list((signal['y'] for signal in signals)))
        z = np.array(list((signal['signal'] for signal in signals)))
        return (x, y, z)

    def plot_points(self, signal_points, ax):
        x, y, z = signal_points
        ax.scatter(x,y,c=z)

        ax.grid(True, which='both')

    def get_measurements(self, bssid, points):
        found = []
        for p in points:
            for m in p.measurements:
                if m.bssid == bssid:
                    found.append({'x': p.x, 'y': p.y, 'signal': m.signal})
                    break
        return found

    def rad_func(self, x, y, z):
        return Rbf(x, y, z, function='linear')

    def draw_rad_fit(self, signal_points, ax, size):
        x = signal_points[0]
        y = signal_points[1]
        z = signal_points[2]
        # print(x, signal_points[0])
        fit_func = self.rad_func(x, y, z)

        x_min, x_max = min(x), max(x)
        y_min, y_max = min(y), max(y)
        xi = np.linspace(x_min, x_max, int(x_max - x_min))  # 0, size[0], size[0] # x_min, x_max, int(size[0])
        yi = np.linspace(y_min, y_max, int(y_max - y_min))  # 0, size[1], size[1] # y_min, y_max, int(size[1])
        X, Y = np.meshgrid(xi, yi)
        zi = fit_func(X, Y)

        ax.contourf(X, Y, zi, 100, alpha=0.5)

        plt.gca().invert_yaxis()
        plt.scatter(x, y, c=z)
        plt.savefig("examples/signal-scatter")
        plt.contourf(X, Y, zi, 100, alpha=0.5)
        plt.savefig("examples/signal-map")
        # ax.scatter(X, Y, zi)
        # ax.imshow(zi, alpha=0.5, extent=(x_min, x_max, y_min, y_min), origin='lower', aspect='auto')

    def draw_fit(self, signal_points, ax, size):
        x, y, z = signal_points

        nx, ny = 200, 200
        xx, yy = np.meshgrid(np.linspace(0, size[0], nx),
                             np.linspace(0, size[1], ny))

        zz = self.interpolate(x, y, z, (xx, yy))

        # m = self.polyfit2d(x, y, z)
        #
        # # Evaluate it on a grid...
        # nx, ny = 100, 100

        # zz = self.polyval2d(xx, yy, m)
        #
        # zz = np.interp(zz, (zz.min(), zz.max()), (min(z), max(z)))

        # Plot
        ax.imshow(zz, extent=(0, size[0], 0, size[1]), alpha=0.5, origin='lower', aspect='auto')

    def draw_figure(self, canvas, figure, loc=(0,0)):
        figure_canvas_agg = FigureCanvasAgg(figure)
        figure_canvas_agg.draw()
        figure_x, figure_y, figure_w, figure_h = figure.bbox.bounds
        figure_w, figure_h = int(figure_w), int(figure_h)
        photo = tk.PhotoImage(master=canvas, width=figure_w, height=figure_h)

        canvas.create_image(loc[0], loc[1], image=photo)

        tkagg.blit(photo, figure_canvas_agg.get_renderer()._renderer, colormode=2)

        return photo