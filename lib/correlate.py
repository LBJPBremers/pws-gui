import numpy as np

class Collerate:
    def __init__(self, calculated_points):
        self.calculated_points = calculated_points

    def n_signal_maps(self):
        sorted_on_signal_maps = {}
        for cp in self.calculated_points:
            if cp.n_signal_maps in sorted_on_signal_maps:
                sorted_on_signal_maps[cp.n_signal_maps].append(cp)
            else:
                sorted_on_signal_maps[cp.n_signal_maps] = [cp]

        stats_per_n_signal_maps = {}
        for n, cps in sorted_on_signal_maps.items():
            stats_per_n_signal_maps[n] = self.stats_on_group(cps)

        return stats_per_n_signal_maps


    def stats_on_group(self, calculated_points):
        deviations = [cp.deviation for cp in calculated_points if cp.success]
        avg = std = min = max = np.NaN
        if len(deviations) > 1:
            avg = np.average(deviations)
            std = np.std(deviations)
            min = np.min(deviations)
            max = np.max(deviations)

        success_rate = len(deviations) / len(calculated_points)

        return Stats(avg, std, min, max, success_rate)


class Stats:
    def __init__(self, avg, std, min, max, success_rate):
        self.avg = avg
        self.std = std
        self.min = min
        self.max = max
        self.success_rate = success_rate
