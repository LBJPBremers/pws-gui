from lib.deviation import Deviation
from lib.reader import PointManager
from lib.correlate import Collerate
import matplotlib.pyplot as plt
import collections
import numpy as np

def plot_stats_per_n_signal_maps(stats):
    ordered_stats = collections.OrderedDict(sorted(stats.items()))

    x = []
    y = []
    e = []
    for n, s in ordered_stats.items():
        x.append(n)
        y.append(s.avg)
        e.append(s.std)

    plt.errorbar(x, y, e, linestyle='None', marker='o')
    plt.show()

def plot_calc_points_on_map(calc_points, map, size):
    img = plt.imread(map)
    plt.imshow(img, origin='lower', extent=(0, size[0], 0, size[1]))

    x = np.array([cp.pos[0] for cp in calc_points if cp.success])
    y = np.array([cp.pos[1] for cp in calc_points if cp.success])
    e = np.array([cp.deviation for cp in calc_points if cp.success])
    u = np.array([cp.diff_x for cp in calc_points if cp.success])
    v = np.array([cp.diff_y for cp in calc_points if cp.success])

    plt.gca().invert_yaxis()
    # plt.scatter(x, y, c=e)
    plt.quiver(x, y, u, v, e)

    plt.tricontourf(x, y, e)
    plt.show()

def print_stats(calc_points):
    deviations = [cp.deviation for cp in calc_points if cp.success]
    print(len(deviations))
    avg = np.average(deviations)
    std = np.std(deviations)
    min = np.min(deviations)
    max = np.max(deviations)
    print("{:.2f}m avg, {:.2f}m std, {:.2f}m min, {:.2f}m max".format(avg, std, min, max))


deviation = Deviation()
point_manager = PointManager()

size = (76.1, 67)
point_manager.open('cb2.pws')

points = list(point_manager.points.values())
points = points[:20]
calculated_points = deviation.mean_deviation(points, size, 2)
calculated_points = deviation.remove_outliers(calculated_points)

plot_calc_points_on_map(calculated_points, 'plattegrond.jpg', size)

correlate = Collerate(calculated_points)
stats = correlate.n_signal_maps()
plot_stats_per_n_signal_maps(stats)

print_stats(calculated_points)