import tkinter as tk
from tkinter import ttk
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from lib.fitting import Fitting

class SignalmapView(ttk.LabelFrame):
    def __init__(self, master, point_manager, size, picture):
        ttk.LabelFrame.__init__(self, master, text='Signal map viewer')

        self.point_manager = point_manager
        self.size = size
        self.picture = picture

        self.fitting = Fitting()

        self.canvas_w, self.canvas_h = (400, 400)

        self.build_canvas()


    def build_canvas(self):
        self.canvas = tk.Canvas(self, width=self.canvas_w, height=self.canvas_h)
        self.canvas.pack(fill=tk.BOTH)

        # X = np.linspace(0, 2 * np.pi, 50)
        # Y = np.sin(X)
        #
        # self.figure = mpl.figure.Figure(figsize=(2,1))
        # ax = self.figure.add_axes([0,0,1,1])
        # ax.plot(X,Y)
        #
        # fig_x, fig_y = 100, 100
        # self.fig_photo = self.fitting.draw_figure(self.canvas, self.figure, loc=(fig_x, fig_y))

    def draw_signals(self, signals):
        signal_points = self.fitting.explode_signals(signals)
        dpi = 90
        fig = mpl.figure.Figure(figsize=(400/dpi, 400/dpi), dpi=dpi)
        ax = fig.add_axes([0.10,0.05,0.90,0.95])
        ax.set_xlim([0, self.size[0]])
        ax.set_ylim([0, self.size[1]])
        ax.invert_yaxis()
                
        self.fitting.plot_points(signal_points, ax)
        self.fitting.draw_rad_fit(signal_points, ax, self.size)
        self.plot_points(self.point_manager.points.values(), ax)
        self.draw_map(ax)
        # self.fitting.draw_fit(signal_points, ax, self.size)

        self.signal_photo = self.fitting.draw_figure(self.canvas, fig, (200, 200))

    def draw_map(self, ax):
        img = plt.imread(self.picture)
        ax.imshow(img, origin='lower', extent=(0, self.size[0], 0, self.size[1]))


    def plot_points(self, points, ax):
        x = list((p.x for p in points))
        y = list((p.y for p in points))
        ax.scatter(x, y, c='black', alpha=0.1)

