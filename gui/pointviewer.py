import tkinter as tk
from tkinter import ttk

from lib.point import Point

class PointViewer(ttk.LabelFrame):
    def __init__(self, master, point_manager):
        ttk.LabelFrame.__init__(self, master, text='Point inspector')

        self.build_fields()
        self.build_buttons()
        self.build_creator()
        self.build_delete()

        self.point_manager = point_manager
        self.point_manager.listen_for_select(self.on_select)

    def build_fields(self):

        self.x_var = tk.DoubleVar()
        self.x_label = tk.Label(self, text='X:').grid(row=0, column=0)
        self.x_entry = tk.Spinbox(self, textvariable=self.x_var).grid(row=0, column=1)

        self.y_var = tk.DoubleVar()
        self.y_label = tk.Label(self, text='Y:').grid(row=1, column=0)
        self.y_entry = tk.Spinbox(self, textvariable=self.y_var).grid(row=1, column=1)

    def build_buttons(self):
        self.set_button = tk.Button(self, text='Update position', command=self.set_pos).grid(row=2, column=0, columnspan=2)

    def build_creator(self):
        self.add_x_var = tk.DoubleVar()
        self.add_x_label = tk.Label(self, text='X+').grid(row=3, column=0)
        self.add_x_entry = tk.Spinbox(self, textvariable=self.add_x_var).grid(row=3, column=1)

        self.add_y_var = tk.DoubleVar()
        self.add_y_label = tk.Label(self, text='Y+').grid(row=4, column=0)
        self.add_y_entry = tk.Spinbox(self, textvariable=self.add_y_var).grid(row=4, column=1)

        self.create_button = tk.Button(self, text='Create new from reference', command=self.create_point).grid(row=5, column=0, columnspan=2)

    def build_delete(self):
        self.delete_button = tk.Button(self, text='Delete', command=self.on_delete).grid(row=6, column=0, columnspan=2)

    def on_delete(self):
        if hasattr(self, 'point'):
            self.point_manager.remove(self.point.get_id())

    def set_pos(self):
        if hasattr(self, 'point'):
            self.point.x = self.x_var.get()
            self.point.y = self.y_var.get()
            self.point_manager.add(self.point)

    def on_select(self, id):
        point = self.point_manager.points[id]
        self.x_var.set(point.x)
        self.y_var.set(point.y)
        self.point = point

    def create_point(self):
        if hasattr(self, 'point'):
            x_add = self.add_x_var.get()
            y_add = self.add_y_var.get()

            x = self.point.x + x_add
            y = self.point.y + y_add
            point = Point(x, y, [])
            self.point_manager.add(point)
            self.point_manager.set_selected(point.get_id())