import tkinter as tk
from tkinter import ttk
from PIL import Image
from PIL import ImageTk

class Map_selector_popup():

    def __init__(self, file, callback):
        self.win = tk.Toplevel()
        self.win.wm_title("Map selector")

        self.file = file
        self.callback = callback

        self.build_canvas()
        self.build_scales()
        self.build_buttons()
        self.build_referencepoint_list()

        self.win.mainloop()

    def build_canvas(self):
        self.canvas = tk.Canvas(self.win, width=500, height=500, bg='white')
        self.canvas.grid(row=0, column=0)

        self.original_image = Image.open(self.file)
        width, height = self.original_image.size
        self.scale = min(500 / width, 500, 500 / height)
        width = width * self.scale
        height = height * self.scale
        self.original_image.thumbnail((width, height), Image.ANTIALIAS)

        self.image = ImageTk.PhotoImage(self.original_image)
        self.canvas_image = self.canvas.create_image(500/2, 500/2, image=self.image, anchor=tk.CENTER)

    def build_scales(self):
        self.width_scale_var = tk.DoubleVar()
        self.width_scale_entry = tk.Entry(self.win, textvariable=self.width_scale_var)
        self.width_scale_entry.grid(row=1, column=0)

        self.height_scale_var = tk.DoubleVar()
        self.height_scale_entry = tk.Entry(self.win, textvariable=self.height_scale_var)
        self.height_scale_entry.grid(row=0, column=1)

    def build_buttons(self):
        self.save_button = tk.Button(self.win, text='Save', command=self.save)
        self.save_button.grid(row=1, column=1)

    def save(self):
        self.callback(self.width_scale_var.get(), self.height_scale_var.get(), self.file)
        self.win.destroy()

    def build_referencepoint_list(self):
        self.referencepoint_list = tk.Listbox(self.win).grid(row=0, column=2)
