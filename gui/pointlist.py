import tkinter as tk
from tkinter import ttk

class PointList(ttk.LabelFrame):
    def __init__(self, master, point_manager):
        ttk.LabelFrame.__init__(self, master, text='Point list')

        self.point_manager = point_manager
        self.point_manager.listen(self.update_points)
        self.build_list()
        self.points_in_list = {}
        self.length = 0
        self.selected = ''

    def build_list(self):
        self.scrollbar = tk.Scrollbar(self)
        self.scrollbar.pack(side=tk.RIGHT, fill = tk.Y)

        self.list_box = tk.Listbox(self, name='pointlist', yscrollcommand = self.scrollbar.set)
        self.list_box.bind('<<ListboxSelect>>', self.on_select)
        self.list_box.pack(side = tk.LEFT, fill = tk.BOTH)

        self.scrollbar.config(command=self.list_box.yview)

    def on_select(self, event):
        index = int(event.widget.curselection()[0])
        point_id = self.points_in_list[index].get_id()
        self.point_manager.set_selected(point_id)


    def update_points(self, points):
        self.points_in_list.clear()
        self.list_box.delete(0, self.length)
        i = 0
        for point in points:
            self.points_in_list[i] = point
            x = round(point.x, 2)
            y = round(point.y, 2)
            self.list_box.insert(i, 'x:{}, y{} => {}'.format(x, y, (len(point.measurements) > 0)))
            i += 1

        self.length = i
