import tkinter as tk
from tkinter import ttk
from lib.scanner import Scanner
from lib.localizing import Localizing

class Localizer(ttk.LabelFrame):
    def __init__(self, master, point_manager):
        ttk.LabelFrame.__init__(self, master, text='Localizer')
        self.point_manager = point_manager

        self.scanner = Scanner()
        self.localizing = Localizing()

        self.build_buttons()
        self.build_list()


    def build_buttons(self):
        self.start_button = tk.Button(self, text='Localize', command=self.start_measurement)
        self.start_button.pack()

    def build_list(self):
        self.scrollbar = tk.Scrollbar(self)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        self.list_label = tk.Label(self, text='Measurements').pack()
        self.list = tk.Listbox(self, width=40, yscrollcommand=self.scrollbar.set)
        self.list.pack(side=tk.LEFT, fill=tk.BOTH)

        self.scrollbar.config(command = self.list.yview)

    def start_measurement(self):
        results = self.scanner.scan(self.scanner.get_interface())
        signals = {}
        for r in results:
            signals[r.bssid] = r.signal

        self.localizing.set_points(self.point_manager.points.values())
        # self.localizing.build_signal_maps(signals)