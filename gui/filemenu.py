import tkinter as tk
from tkinter import ttk

class FileMenu(ttk.LabelFrame):
    def __init__(self, master, point_manager):
        ttk.LabelFrame.__init__(self, master, text='Filemenu')

        self.point_manager = point_manager

        self.build_interface()

    def build_interface(self):
        self.file_var = tk.StringVar()
        self.file_entry = tk.Entry(self, textvariable=self.file_var).pack()
        self.save_button = tk.Button(self, text='Save file', command=self.save_file).pack(fill=tk.BOTH)
        self.open_button = tk.Button(self, text='Open file', command=self.open_file).pack(fill=tk.BOTH)

    def save_file(self):
        self.point_manager.save(self.file_var.get())

    def open_file(self):
        self.point_manager.open(self.file_var.get())