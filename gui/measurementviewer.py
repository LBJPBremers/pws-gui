import tkinter as tk
from tkinter import ttk
from lib.scanner import Scanner
from lib.measurement import Measurement
from lib.localizing import Localizing
import math
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from matplotlib import cm

class MeasurementViewer(ttk.LabelFrame):
    def __init__(self, master, point_manager, map_size, draw_localize_rect):
        ttk.LabelFrame.__init__(self, master, text='Measurements')

        self.point_manager = point_manager
        self.point_manager.listen_for_select(self.set_list)
        self.map_size = map_size

        self.draw_localize_rect = draw_localize_rect

        self.build_buttons()
        self.build_list()

        self.scanner = Scanner()
        self.localizing = Localizing()

        self.length = 0

    def build_list(self):
        self.scrollbar = tk.Scrollbar(self)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        self.list_label = tk.Label(self, text='Measurements').pack()
        self.list = tk.Listbox(self, width=40, yscrollcommand=self.scrollbar.set)
        self.list.pack(side=tk.LEFT, fill=tk.BOTH)

        self.scrollbar.config(command = self.list.yview)

    def build_buttons(self):
        self.start_button = tk.Button(self, text='Start', command=self.start_measurement)
        self.start_button.pack()

        self.localize_button = tk.Button(self, text='Localize', command=self.localize)
        self.localize_button.pack()

    def localize(self):
        id = self.point_manager.selected
        signals = {}
        if id is not '':
            point = self.point_manager.points[id]
            for m in point.measurements:
                signals[m.bssid] = m.signal
        else:
            return

        # set points for calculations
        points = list(self.point_manager.points.values())
        self.localizing.set_points(points, exclude_id=id)

        # find rectangle and build signal maps in rectangle
        signal_maps, grid_x, grid_y, rectangle, rectangles = self.localizing.build_intersect_signal_maps(signals, self.map_size)

        # calculate total diff map
        total_diff, diff_maps = self.localizing.build_diff_map(signal_maps, signals)

        i = 0
        inverted_colormap = ListedColormap(cm.viridis.colors[::-1])
        for bssid, diff_map in diff_maps.items():
            i+=1
            plt.contourf(grid_x, grid_y, diff_map.reshape(grid_x.shape), cmap=inverted_colormap)
            plt.savefig('examples/diffs/{}.png'.format(i))

        plt.contourf(grid_x, grid_y, total_diff.reshape(grid_x.shape), cmap=inverted_colormap)
        plt.savefig('examples/diffs/total-diffs.png')

        # get lowest cost
        lowest_point = self.localizing.get_lowest_coordinate(total_diff, grid_x, grid_y)
        point = self.point_manager.points[id]
        diff_x = point.x - lowest_point[0]
        diff_y = point.y - lowest_point[1]
        diff = math.sqrt(diff_x**2 + diff_y**2)
        print(diff)

        # reshape total diff to plot on graph
        total_diff = total_diff.reshape(grid_x.shape)

        self.draw_localize_rect(id, rectangle, grid_x, grid_y, total_diff, rectangles)


    def start_measurement(self):
        self.start_button.config(state=tk.DISABLED, text='Scanning')
        results = self.scanner.scan(self.scanner.get_interface())
        self.start_button.config(state=tk.NORMAL, text='Start')

        id = self.point_manager.selected
        if id is not '':
            point = self.point_manager.points[id]
            point.measurements = []
            for r in results:
                measurement = Measurement(r.ssid, r.bssid, r.signal)
                point.measurements.append(measurement)
            self.point_manager.add(point)
            self.set_list(id)

    def set_list(self, id):
        measurements = self.point_manager.points[id].measurements
        self.list.delete(0, self.length)
        i = 0
        for measurement in measurements:
            self.list.insert(i, '{} ({}): {}dBm'.format(measurement.bssid, measurement.ssid, measurement.signal))
            i += 1

        self.length = i