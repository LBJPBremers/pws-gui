import tkinter as tk
from tkinter import ttk

from PIL import Image
from PIL import ImageTk

class MapViewer(ttk.Frame):
    def __init__(self, master, file, real_x_size, real_y_size, point_manager):
        ttk.Frame.__init__(self, master)

        self.canvas_width, self.canvas_height = (600,600)

        self.real_x_size = real_x_size
        self.real_y_size = real_y_size

        self.file = file

        self.point_manager = point_manager
        self.point_manager.listen(self.redraw_points)
        self.point_manager.listen_for_select(self.set_selected)

        self.build_map()

        self.canvas_points = []
        self.selected_id = ''

    def set_selected(self, id):
        self.selected_id = id
        self.redraw_points(self.point_manager.points.values())

    def mmove(self, event):
        x = round((event.x - self.image_x_pos) / self.real_scale_x, 2)
        y = round((event.y - self.image_y_pos) / self.real_scale_y, 2)
        self.pos_label_var.set('x:{}, y:{}'.format(x, y))

    def redraw_points(self, points):
        for point in self.canvas_points:
            self.canvas.delete(point)

        for point in points:
            canvas_x_pos = self.image_x_pos + (point.x * self.real_scale_x)
            canvas_y_pos = self.image_y_pos + (point.y * self.real_scale_y)

            if point.get_id() is self.selected_id:
                color = 'yellow'
            elif len(point.measurements) > 0:
                color = 'black'
            else:
                color = 'red'
            self.canvas_points.append(self.canvas.create_oval(canvas_x_pos - self.point_size_x, canvas_y_pos - self.point_size_y, canvas_x_pos + self.point_size_x, canvas_y_pos + self.point_size_y, fill = color))


    def build_map(self):
        self.canvas = tk.Canvas(self, width=self.canvas_width, height=self.canvas_height, bg='white')
        self.canvas.pack()
        self.canvas.bind('<Motion>', self.mmove)

        self.load_image(self.file)

        width, height = self.image.size
        self.image_x_pos = (self.canvas_width - width) / 2
        self.image_y_pos = (self.canvas_height - height) / 2
        self.canvas_image = self.canvas.create_image(self.image_x_pos, self.image_y_pos, image=self.tk_image, anchor=tk.NW)

        self.pos_label_var = tk.StringVar()
        self.pos_label = tk.Label(self, textvariable=self.pos_label_var).pack()

    def load_image(self, file):
        self.original_image = Image.open(file)
        self.reset_size()

    def reset_size(self):
        self.image = self.original_image.copy()

        width, height = self.image.size
        self.map_ratio_scale = min(self.canvas_width / width, self.canvas_height / height)
        width = width * self.map_ratio_scale
        height = height * self.map_ratio_scale

        self.real_scale_x = width / self.real_x_size
        self.real_scale_y = height / self.real_y_size

        self.point_size_x = self.real_scale_x * 0.5
        self.point_size_y = self.real_scale_y * 0.5

        self.image.thumbnail((width, height), Image.ANTIALIAS)
        self.tk_image = ImageTk.PhotoImage(self.image)


