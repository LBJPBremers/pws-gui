import tkinter as tk
from tkinter import ttk
from matplotlib import cm
from matplotlib.colors import ListedColormap
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from lib.fitting import Fitting


class Graphmap(ttk.LabelFrame):
    def __init__(self, master, point_manager, size, picture):
        ttk.LabelFrame.__init__(self, master, text='Graph map')

        self.point_manager = point_manager
        self.size = size
        self.picture = picture

        self.point_manager.listen(self.update_points)
        self.point_manager.listen_for_select(self.draw_selected)

        self.canvas_w, self.canvas_h = (600, 600)

        self.fitting = Fitting()

        self.build_canvas()

    def build_canvas(self):
        self.canvas = tk.Canvas(self, width=self.canvas_w, height=self.canvas_h)
        self.canvas.pack(fill=tk.BOTH)

        self.img = plt.imread(self.picture)

        self.update_points([])

    def build_ax(self):
        dpi = 90
        self.fig = mpl.figure.Figure(figsize=(self.canvas_w / dpi, self.canvas_h / dpi), dpi=dpi)
        self.ax = self.fig.add_axes([0.05, 0, 0.95, 1])
        self.ax.set_xlim([0, self.size[0]])
        self.ax.set_ylim([0, self.size[1]])
        self.ax.invert_yaxis()
        self.ax.grid(True, which='both')

    def render(self):
        self.photo = self.fitting.draw_figure(self.canvas, self.fig, (self.canvas_w / 2, self.canvas_h / 2))

    def draw_all(self, signals):
        self.build_ax()
        self.draw_background()
        self.draw_points(list(self.point_manager.points.values()), alpha=0.2)
        self.draw_signals(signals)
        self.render()

    def draw_localize_rect(self, id, rectangle, grid_x, grid_y, total_diff, rectangles):
        self.build_ax()
        self.draw_background()
        self.draw_selected(id)

        for r in rectangles:
            rect = patches.Rectangle((r[:2]), r[2], r[3], linewidth=1, edgecolor='r', facecolor='none')
            self.ax.add_patch(rect)

        rect = patches.Rectangle((rectangle[:2]), rectangle[2], rectangle[3], linewidth=3, edgecolor='r', fill=True, alpha=0.5)
        self.ax.add_patch(rect)

        inverted_colormap = ListedColormap(cm.viridis.colors[::-1])
        self.ax.contourf(grid_x, grid_y, total_diff, 50, alpha=0.5, cmap=inverted_colormap)
        self.render()

    def draw_multi_signal(self, bssids):
        self.build_ax()
        self.draw_background()
        self.draw_points(list(self.point_manager.points.values()), alpha=0.2)

        for bssid, signals in bssids.items():
            self.draw_signals(signals)

        self.render()

    def draw_selected(self, id):
        self.build_ax()
        self.draw_background()
        self.draw_points(list(self.point_manager.points.values()))
        point = self.point_manager.points[id]
        self.ax.scatter(point.x, point.y, c='y')
        self.render()

    def update_points(self, points):
        self.build_ax()
        self.draw_background()
        self.draw_points(points)
        self.render()

    def draw_background(self):
        self.ax.imshow(self.img, origin='lower', extent=(0, self.size[0], 0, self.size[1]))

    def draw_signals(self, signals):
        signal_points = self.fitting.explode_signals(signals)
        self.fitting.plot_points(signal_points, self.ax)
        self.fitting.draw_rad_fit(signal_points, self.ax, self.size)

    def draw_points(self, points, alpha=0.5):
        x = list((p.x for p in points))
        y = list((p.y for p in points))
        self.ax.scatter(x, y, c='black', alpha=alpha)
