import tkinter as tk
from tkinter import ttk
from lib.fitting import Fitting


class APList(ttk.LabelFrame):
    def __init__(self, master, point_manager, on_select):
        ttk.LabelFrame.__init__(self, master, text='AP list')

        self.fitting = Fitting()

        self.on_select_callback = on_select

        self.point_manager = point_manager
        self.point_manager.listen(self.update_list)
        self.build_list()
        self.aps_list = []
        self.length = 0
        self.selected = ''

    def build_list(self):
        self.scrollbar = tk.Scrollbar(self)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        self.list_box = tk.Listbox(self, name='ap list', yscrollcommand=self.scrollbar.set, width=40)
        self.list_box.bind('<<ListboxSelect>>', self.on_select)
        self.list_box.pack(side=tk.LEFT, fill=tk.BOTH)

        self.scrollbar.config(command=self.list_box.yview)

    def on_select(self, event):
        index = int(event.widget.curselection()[0])
        bssid = self.aps_list[index]
        signals = self.get_measurements(bssid)
        if len(signals) < 2:
            return
        self.on_select_callback(signals)

    def get_measurements(self, bssid):
        points = self.point_manager.points.values()
        return self.fitting.get_measurements(bssid, points)


    def update_list(self, points):
        # todo: update signal map
        ap_count = {}
        for point in points:
            for m in point.measurements:
                if m.bssid in ap_count:
                    ap_count[m.bssid] += 1
                else:
                    ap_count[m.bssid] = 1

        self.list_box.delete(0, len(self.aps_list))
        self.aps_list.clear()
        for bssid, count in ap_count.items():
            self.aps_list.append(bssid)
            self.list_box.insert(len(self.aps_list) - 1, '{} ({})'.format(bssid, ap_count[bssid]))
