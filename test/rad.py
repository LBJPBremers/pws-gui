from scipy.interpolate import Rbf
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt
x, y, z = np.random.rand(3, 50)
rbfi = Rbf(x, y, z, function='cubic')  # radial basis function interpolator instance
xi = np.linspace(0, 1, 20)
yi = np.linspace(0,1,20)
X, Y = np.meshgrid(xi, yi)
zi = rbfi(X, Y)   # interpolated values


fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(x, y, c=z)
ax.contourf(X, Y, zi, alpha=0.5)
plt.show()