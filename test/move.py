import json

class Measurement():
    def __init__(self, ssid, bssid, signal):
        self.ssid = ssid
        self.bssid = bssid
        self.signal = signal

import uuid

class Point():
    def __init__(self, x, y, measurements):
        self.x = x
        self.y = y
        self.measurements = measurements
        self.__id = str(uuid.uuid4())

    def get_id(self):
        return self.__id

def save(file, points):
    root = []
    for point in points:
        m = []
        for measurement in point.measurements:
            m.append({
                'ssid': measurement.ssid,
                'bssid': measurement.bssid,
                'signal': measurement.signal
            })
        p = {
            'x': point.x,
            'y': point.y,
            'measurements': m
        }
        root.append(p)

    with open(file, 'w') as outf:
        json.dump(root, outf)

def read(file):
    with open(file) as inf:
        data = json.load(inf)
        points = []
        for p in data:
            measurements = []
            for m in p['measurements']:
                measurement = Measurement(m['ssid'], m['bssid'], m['signal'])
                measurements.append(measurement)
            point = Point(p['x'], p['y'], measurements)
            points.append(point)

    return points

def min_max(points):
    max_s = -100
    min_s = 100
    for p in points:
        for m in p.measurements:
            max_s = max(max_s, m.signal)
            min_s = min(min_s, m.signal)

    return (min_s, max_s)

points = read('../cb2.json')

new_points = []
for p in points:
    if p.x != 23.0 and p.x != 25.0:
        new_points.append(p)

save('../cb2.json.test', new_points)

print(len(new_points), len(points))